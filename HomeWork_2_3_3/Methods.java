import java.util.Scanner;

public class Main {

    public static double pow(double x, int y) {
        double f = 1;
        for (int i = 1; i <= y; i++) {
            f = f * x;
        }
        return f;
    }

    public static double integral(double a, double b, int n, int ys) {
        double h = (b - a) / n;
        double answer = 0;
        for (double i = a; i < b; i = i + h) {
            double si = h * pow(i, ys);
            answer = answer + si;
        }
        return answer;
    }

    public static double simpson(double a, double b, int n, int ys) {
        double h = (b - a) / n;
        double answer = 0;
        for (double x = a; x < b; x = x + 2 * h) {
            double summ = pow(x - h, ys) + 4 * pow(x, ys) + pow(x + h, ys);
            answer = answer + summ;
        }
        return h * answer / 3;
    }


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the function ends");

        int a = scanner.nextInt();
        int b = scanner.nextInt();

        int[] ns = {100, 1000, 10000, 100000, 1000000};
        int[] ys = {2, 3, 4, 5, 6, 7};

        for (int i = 0; i < ys.length - 1; i++) {
            for (int j = 0; j < ns.length - 1; j++) {
                System.out.println("Simpson method - " + ns[j] + " - x^" + ys[i] + "=" + simpson(a, b, ns[j], ys[i]));
            }
        }

        for (int i = 0; i < ys.length - 1; i++) {
            for (int j = 0; j < ns.length - 1; j++) {
                System.out.println("Method of rectangles - " + ns[j] + " - x^" + ys[i] + "=" + integral(a, b, ns[j], ys[i]));
            }
        }


    }
}

