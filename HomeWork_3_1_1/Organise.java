import java.util.Scanner;

public class Chars {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char text[][];
        System.out.println("Enter count of String");
        int n = scanner.nextInt();
        scanner.nextLine();
        text = new char[n][];
        for (int i = 0; i < text.length; i++) {
            text[i] = scanner.nextLine().toCharArray();
        }
        print(text);
        System.out.println();
        print(organise(text));

    }

    public static int stringCompare(char[] s1, char[] s2) {
        int lenString;
        int compare = 0;
        if (s1.length <= s2.length) {
            lenString = s1.length;
        } else {
            lenString = s2.length;

        }


        for (int i = 0; i < lenString; i++) {
            if (s1[i] > s2[i]) {
                compare = -1;
                break;
            } else if (s1[i] < s2[i]) {
                compare = 1;
                break;
            } else {
                compare = 0;
            }
            if (compare == 0) {
                if (s1.length < s2.length) {
                    compare = 1;
                } else if (s1.length > s2.length) {
                    compare = -1;

                }
            }

        }
        return compare;
    }

    public static void print(char[][] inArr) {
        for (int i = 0; i < inArr.length; i++) {
            for (int j = 0; j < inArr[i].length; j++) {
                System.out.print(inArr[i][j]);
            }
            System.out.println();
        }

    }

    public static char[][] organise(char[][] inArr) {
        char[] temp;
        for (int i = 0; i < inArr.length; i++) {
            for (int j = i + 1; j < inArr.length; j++) {
                if (stringCompare(inArr[i], inArr[j]) == -1) {
                    temp = inArr[i];
                    inArr[i] = inArr[j];
                    inArr[j] = temp;
                }
            }
        }
        return inArr;
    }

}


