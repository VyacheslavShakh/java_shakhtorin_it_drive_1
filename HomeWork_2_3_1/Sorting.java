public class Main {
    public static void sorting(int arr[]) {
        for (int i = 0; i <= arr.length - 1; i++) {
            int min = arr[i];
            for (int j = i; j <= arr.length - 1; j++) {
                if (arr[j] < min) {
                    int temp = min;
                    min = arr[j];
                    arr[j] = temp;

                }
                arr[i] = min;
            }
        }
    }
}