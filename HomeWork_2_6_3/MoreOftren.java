import java.util.Scanner;

public class Chars {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your String");
        char str1[] = scanner.nextLine().toCharArray();


        System.out.print(toInt(moreOftenEllement(str1)));


    }

    public static int toInt(char[] arr) {
        int value = 0;

        for (int i = 0; i < arr.length; i++) {
            value = (value + arr[i] - 48) * 10;
        }

        return value / 10;
    }

    public static char[] moreOftenEllement(char[] inArr) {
        char[] valueArr = new char[3];
        int[] numOut = new int[3];
        int[] numArr = new int[65536];


        for (int i = 0; i < inArr.length; i++) {
            numArr[(int) inArr[i]]++;
        }

        for (int i = 0; i < numArr.length; i++) {
            if (numArr[i] > 0) {

                if (numArr[i] > numOut[0]) {
                    valueArr[2] = valueArr[1];
                    numOut[2] = numOut[1];
                    valueArr[1] = valueArr[0];
                    numOut[1] = numOut[0];
                    valueArr[0] = (char) i;
                    numOut[0] = numArr[i];

                } else if ((numArr[i] <= numOut[0]) && (numArr[i] > numOut[1])) {
                    valueArr[2] = valueArr[1];
                    numOut[2] = numOut[1];
                    valueArr[1] = (char) i;
                    numOut[1] = numArr[i];
                } else if ((numArr[i] <= numOut[1]) && (numArr[i] > numOut[2])) {
                    valueArr[2] = (char) i;
                    numOut[2] = numArr[i];
                }
            }
        }


        return valueArr;
    }
}


