package additionally.five;


import java.util.Scanner;


public class Journal {
    private ClassWork[] classWorks = new ClassWork[40];
    private Scanner scanner = new Scanner(System.in);
    private Student[] students = new Student[10];
    private int countClassWorks = 0;
    private int countStudents = 0;

    void enterListStudents() {
        System.out.println("Enter students name for END entering (...):");
        for (int i = 0; i < students.length; i++) {
            String name = scanner.next();

            if (name.equals("...")) {
                scanner.nextLine();
                break;
            }

            if (name.length() > 10) {
                name = name.substring(0, 10);
            }
            students[i] = new Student(name);
            countStudents++;
            scanner.nextLine();
        }
    }

    void createClassWorks(Timetable timetable, StudyMonth studyMonth) {

        for (int i = 0; i < studyMonth.getStadyDayOfMonth().length; i++) {
            for (int j = 0; j < timetable.getCountLessons(); j++) {
                if (studyMonth.getStadyDayOfMonth()[i].equals(timetable.getLessons()[j].getDay())) {
                    classWorks[countClassWorks] = new ClassWork(timetable.getLessons()[j], i + 1);
                    countClassWorks++;
                }
            }
        }
    }

    void enterTraffic() {
        System.out.println("Enter traffic Students (NAME TIME DATE HERE/NOT_HERE)");

        for (int i = 0; i < countStudents * countClassWorks; i++) {

            if (scanner.hasNext("...")) {
                scanner.nextLine();
                break;
            }

            String name = scanner.next();

            if (name.equals("...")) {
                scanner.next();
                break;
            }

            int time = scanner.nextInt();
            if ((1 > time) | (time > 6)) {
                System.out.println("Time is not correct, try again(from 1 to 6)");
                i--;
                continue;
            }
            int date = scanner.nextInt();

            String attending = scanner.next();

            if (attending.equals("HERE")) {

                for (int j = 0; j < countClassWorks; j++) {
                    if ((classWorks[j].getLesson().getTime() == time) && (classWorks[j].getDate() == date)) {
                        for (int k = 0; k < countStudents; k++) {
                            if (name.equals(students[k].getName())) {
                                classWorks[j].setAttending(students[k], k);
                                students[k].setClassPlane(classWorks[j], j);
                            }
                        }
                    }
                }
            }
        }
    }

    void printJournal() {
        System.out.printf("%10s"," ");
        for (int i = 0; i < countClassWorks; i++) {
            System.out.print(classWorks[i].getLesson().getTime() + ":00 ");
            System.out.printf("%4s",classWorks[i].getLesson().getDay() + " ");
            System.out.printf("%3s",classWorks[i].getDate() + "|");
        }
        System.out.println();
        for (int i = 0; i < countStudents; i++) {
            System.out.printf("%10s",students[i].getName());
            for (int j = 0; j < countClassWorks; j++) {
                System.out.printf("%12s",((students[i].getClassPlane(j) != null) ? ("+") : ("-")) + " |");
            }
            System.out.println();
        }

    }
}

