package additionally.five;

public class ClassWork{
    private Lesson lesson;
    private int date;
    private Student[] attending = new Student[10];


    public void setAttending(Student attending, int i) {
        this.attending[i] = attending;
    }

    public ClassWork(Lesson lesson, int date) {
        this.lesson = lesson;
        this.date = date;
    }

    public int getDate() {
        return date;
    }

    public Lesson getLesson() {
        return lesson;
    }
}
