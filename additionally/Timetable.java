package additionally.five;

import java.util.Scanner;

public class Timetable {
    private String name;
    private Lesson[] lessons = new Lesson[10];
    Scanner scanner = new Scanner(System.in);
    int countLessons = 0;

    public Timetable(String name) {
        this.name = name;
    }

    public int getCountLessons() {
        return countLessons;
    }

    public Lesson[] getLessons() {
        return lessons;
    }


    void enterLessons() {
        System.out.println("Enter schedule time (from 1 to 6) and day of week (format MO, TU, WE, THU, FR, SAT, SUN) for END entering (...)");
        for (int i = 0; i < lessons.length; i++) {

            if (scanner.hasNext("...")) {
                scanner.nextLine();
                break;
            }

            String inPut = scanner.nextLine();

            int time = Integer.parseInt(inPut.substring(0, 1));
            if ((1 > time) | (time > 6)) {
                System.out.println("Time is not correct, try again(from 1 to 6)");
                i--;
                continue;
            }
            String day = inPut.substring(2);
            boolean dayCorrect = false;
            for (int j = 0; j < 7; j++) {
                dayCorrect = day.equals(StudyMonth.getDefaultWeek()[j]);
                if (dayCorrect) {
                    break;
                }
            }
            if (!dayCorrect) {
                System.out.println("Day is not correct, try again(format MO, TU, WE, THU, FR, SAT, SUN)");
                i--;
            }

            lessons[i] = new Lesson(day,time);
            countLessons++;
        }
    }
}
