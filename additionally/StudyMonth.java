package additionally.five;

public class StudyMonth {
    private static final String[] defaultWeek = {"MO", "TU", "WE", "THU", "FR", "SAT", "SUN"};
    private String[] firstWeek;
    private String[] stadyDayOfMonth;

    StudyMonth(int firstDay, int monthDays) {

        firstWeek = new String[7];

        for (int i = 0; i < firstWeek.length; i++) {
            if (i + firstDay > firstWeek.length - 1) {
                firstWeek[i] = defaultWeek[i + firstDay - firstWeek.length];
            } else {
                firstWeek[i] = defaultWeek[i + firstDay];
            }
        }

        stadyDayOfMonth = new String[monthDays];

        for (int i = 0; i < monthDays; i++) {
            stadyDayOfMonth[i] = firstWeek[i - i / 7 * 7];
        }
    }

    public static String[] getDefaultWeek() {
        return defaultWeek;
    }

    public String[] getStadyDayOfMonth() {
        return stadyDayOfMonth;
    }
}
