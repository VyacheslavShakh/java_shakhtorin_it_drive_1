package additionally.five;

public class Lesson {
    private String day;
    private int time;

    public Lesson(String day, int time) {
        this.day = day;
        this.time = time;
    }

    public String getDay() {
        return day;
    }



    public int getTime() {
        return time;
    }


}
