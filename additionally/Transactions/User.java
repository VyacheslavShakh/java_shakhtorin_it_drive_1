package additionally.paypal;

public class User {
    private int id;
    private String name;
    private int balance;
    TransactionsLinkedList list;

    public User(String name, int balance) {
        if(balance<0){
            System.out.println("Balance less by 0");
            return;
        }
        this.id = UserIdsGenerator.getInstance().generateID();
        this.name = name;
        this.balance = balance;
        list = new TransactionsLinkedList();
    }

    public String getName() {
        return name;
    }

    public int getBalance() {
        return balance;
    }

    public int getId() {
        return id;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public TransactionsLinkedList getList() {
        return list;
    }

    public void showUser(){
        System.out.println(name);
        System.out.println(id);
        System.out.println(balance);

    }

}
