package additionally.paypal;

public class TransactionsLinkedList implements TransactionsList {
    Transaction first, last;


    @Override
    public void addTransaction(String uuid, User recipient, User sender, boolean input, int sum) {
        Transaction newTransaction = new Transaction(uuid, recipient, sender, input, sum);
        if (this.first == null) {
            this.first = newTransaction;
            this.last = this.first;
        } else {
            Transaction currantTransaction = last;
            currantTransaction.setNext(newTransaction);
            last = currantTransaction.getNext();
        }

    }

    @Override
    public void delTransaction(String id) {
        Transaction currentTransaction = first;
        Transaction previousTransaction = null;
        Transaction nextTransaction = currentTransaction.getNext();
        boolean transactionDeleted = false;
        while (currentTransaction.hasNext()) {
            if (currentTransaction.getUuid().equals(id)) {
                transactionDeleted = true;
                
                if (currentTransaction == first) {
                    first = nextTransaction;
                    break;
                } else if (currentTransaction == last) {
                    last = previousTransaction;
                    previousTransaction.setNext(null);
                    break;
                } else {
                    previousTransaction.setNext(nextTransaction);
                    break;
                }
            }
            previousTransaction = currentTransaction;
            currentTransaction = currentTransaction.getNext();
            nextTransaction = currentTransaction.getNext();
        }
        if (!transactionDeleted) {
            System.out.println("Transaction not exist");
        }
    }

    @Override
    public Transaction[] toArray() {
        int count = 0;
        Transaction currentTransaction = first;
        while (currentTransaction.hasNext()){
            currentTransaction = currentTransaction.getNext();
            count++;
        }

        currentTransaction = first;
        Transaction[] listTransactions = new Transaction[count+1];
        for (int i = 0; i < listTransactions.length; i++){
            listTransactions[i] = currentTransaction;
            currentTransaction = currentTransaction.getNext();
        }
        return listTransactions;
    }

    public Transaction getTransaction (String uuid){
        Transaction currentTransaction = first;
        while (currentTransaction.hasNext()){
            if (currentTransaction.getUuid().equals(uuid)){
                return currentTransaction;
            }
            currentTransaction = currentTransaction.getNext();
        }
        return null;
    }

    public boolean hasTransaction (String uuid){
        boolean has = false;
        Transaction currentTransaction = first;
        while (currentTransaction.hasNext()) {
            if (currentTransaction.getUuid().equals(uuid)){
                has = true;
            }
            currentTransaction = currentTransaction.getNext();
        }
        if (currentTransaction.getUuid().equals(uuid)){
            has = true;
        }
        return has;
    }

}
