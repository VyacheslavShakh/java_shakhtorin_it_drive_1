package additionally.paypal;

public interface TransactionsList {
    void addTransaction(String uuid, User recipient, User sender, boolean input, int sum);
    void delTransaction(String id);
    Transaction[] toArray ();
}
