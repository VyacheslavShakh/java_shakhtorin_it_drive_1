package additionally.paypal;


public interface UserList {
    void addUser(User user);
    User getUserID(int id);
    User getUser(int index);
    int getCountUsers();
}