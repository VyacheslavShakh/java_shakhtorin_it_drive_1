package additionally.paypal;

import java.util.UUID;

public class TransactionsService {

    UsersArrayList usersArrayList;

    public int addUser(String name, int balance) {
        if (usersArrayList == null){
            usersArrayList = new UsersArrayList();
        }
        User user = new User(name, balance);
        usersArrayList.addUser(user);
        return user.getId();
    }

    public UsersArrayList getUsersArrayList() {
        return usersArrayList;
    }

    public int getUserBalance(int idUser) {
        return usersArrayList.getUserID(idUser).getBalance();
    }

    public void createTransaction(int idUserSender, int idUserRecipient, int sum) {
        if (usersArrayList.getUserID(idUserSender) == null) {
            System.out.println("Sender not found, transaction not create");
            return;
        }
        if (usersArrayList.getUserID(idUserRecipient) == null) {
            System.out.println("Recipient not found, transaction not create");
            return;
        }
        if (sum > usersArrayList.getUserID(idUserSender).getBalance()) {
            System.out.println("Sender balance less then transaction sum, transaction not create");
            return;
        }
        if (usersArrayList.getUserID(idUserSender).getList() == null) {
            usersArrayList.getUserID(idUserSender).list = new TransactionsLinkedList();
        }
        String uuid = UUID.randomUUID().toString();
        usersArrayList.getUserID(idUserSender).getList().addTransaction(uuid, usersArrayList.getUserID(idUserSender), usersArrayList.getUserID(idUserRecipient), false, -sum);
        usersArrayList.getUserID(idUserSender).setBalance(usersArrayList.getUserID(idUserSender).getBalance() - sum);
        usersArrayList.getUserID(idUserRecipient).getList().addTransaction(uuid, usersArrayList.getUserID(idUserSender), usersArrayList.getUserID(idUserRecipient), true, sum);
        usersArrayList.getUserID(idUserRecipient).setBalance(usersArrayList.getUserID(idUserRecipient).getBalance() + sum);
    }

    public Transaction[] getTransactionsList(int idUser) {
        return usersArrayList.getUserID(idUser).getList().toArray();
    }

    public Transaction[] checkTransaction() {
        Transaction[] wrongTransactions = new Transaction[100];
        int x = 0;
        for (int i = 0; i < usersArrayList.getCountUsers(); i++) {

            for (int j = 0; j < usersArrayList.getUser(i).getList().toArray().length; j++) {
                String uuid = usersArrayList.getUser(i).getList().toArray()[j].getUuid();
                User thisUser, anotherUser;
                if (usersArrayList.getUser(i) == usersArrayList.getUser(i).getList().toArray()[j].getRecipient()) {
                    thisUser = usersArrayList.getUser(i).getList().toArray()[j].getRecipient();
                    anotherUser = usersArrayList.getUser(i).getList().toArray()[j].getSender();
                } else {
                    anotherUser = usersArrayList.getUser(i).getList().toArray()[j].getRecipient();
                    thisUser = usersArrayList.getUser(i).getList().toArray()[j].getSender();
                }
                if (!anotherUser.getList().hasTransaction(uuid)) {
                    wrongTransactions[x] = usersArrayList.getUser(i).getList().toArray()[j];
                    x++;
                }
            }
        }
        Transaction[] out = new Transaction[x];
        for (int i = 0; i < out.length; i++){
            out[i] = wrongTransactions[i];
        }
        return out;
    }


}
