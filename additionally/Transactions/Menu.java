package additionally.paypal;

public interface Menu {
    void addUser();
    void showUserBalance();
    void remittance();
    void showUserTransactionList();
    void delTransaction();
    void falseTransaction();
}
