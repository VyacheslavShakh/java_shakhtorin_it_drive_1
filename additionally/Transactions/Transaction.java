package additionally.paypal;


public class Transaction {
    private String uuid;
    private User recipient;
    private User sender;
    private boolean input; // input transaction - true ; output transaction - false
    private int sum;
    Transaction next;

    public User getRecipient() {
        return recipient;
    }

    public Transaction(String uuid, User sender, User recipient, boolean input, int sum) {
        this.uuid = uuid;
        this.recipient = recipient;
        this.sender = sender;
        this.input = input;
        this.sum = sum;

    }

    public User getSender() {
        return sender;
    }

    public int getSum() {
        return sum;
    }

    public Transaction getNext() {
        return next;
    }

    public void setNext(Transaction next) {
        this.next = next;
    }

    public boolean hasNext(){
        return this.next != null;
    }

    public String getUuid() {
        return uuid;
    }
}
