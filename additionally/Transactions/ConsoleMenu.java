package additionally.paypal;

import java.util.Scanner;

public class ConsoleMenu implements Menu{

    TransactionsService transactionsService;
    Scanner scanner = new Scanner(System.in);


    ConsoleMenu(TransactionsService transactionsService){

        this.transactionsService = transactionsService;

    }

    public void start(){
        boolean nextStep = true;
        while (nextStep){
            System.out.println("-------------------------");
            printMenu();
            int inPut = scanner.nextInt();
            switch (inPut) {
                case (1):
                    addUser();
                    break;
                case (2):
                    showUserBalance();
                    break;
                case (3):
                    remittance();
                    break;
                case (4):
                    showUserTransactionList();
                    break;
                case (5):
                    delTransaction();
                    break;
                case (6):
                    falseTransaction();
                    break;
                case (7):
                    nextStep = false;
                    break;
                default:
                    System.out.println("Wrong command");
                    break;
            }
        }
    }

    public void printMenu(){
        System.out.println("1. Add User");
        System.out.println("2. View the balance of the user");
        System.out.println("3. Make a transfer");
        System.out.println("4. View all translations of user");
        System.out.println("5. Delete transaction with ID");
        System.out.println("6. Check the correctness of transactions");
        System.out.println("7. End work apps");
        System.out.print("->");
    }

    @Override
    public void addUser() {
        System.out.println("Enter name and balance user");
        String name = scanner.next();
        int balance = scanner.nextInt();
        int userId = transactionsService.addUser(name, balance);
        System.out.println("User is added with id " + userId);
    }

    @Override
    public void showUserBalance() {
        System.out.println("Enter user id");
        int userId = scanner.nextInt();
        String name = transactionsService.getUsersArrayList().getUserID(userId).getName();
        int balance = transactionsService.getUsersArrayList().getUserID(userId).getBalance();
        System.out.println(name + " - " + balance);
    }

    @Override
    public void remittance() {
        System.out.println(" Enter id - senders, id - recipients, sum of transaction ");
        int idSender = scanner.nextInt();
        int idRecipient = scanner.nextInt();
        int sum = scanner.nextInt();
        transactionsService.createTransaction(idSender, idRecipient, sum);
        System.out.println("Transaction completed");
    }

    @Override
    public void showUserTransactionList() {
        System.out.println("Enter user id");
        int idUser = scanner.nextInt();
        Transaction[] transactions = transactionsService.getUsersArrayList().getUserID(idUser).getList().toArray();
        for (int i = 0; i < transactions.length; i++){
            User user;
            if (transactions[i].getRecipient().getId() == idUser){
                 user = transactions[i].getSender();
            }else {
                user = transactions[i].getRecipient();
            }
            System.out.println( " to " + user.getName() + "(id = " + user.getId() + ") " + transactions[i].getSum() + " with id = " + transactions[i].getUuid());
        }
    }

    @Override
    public void delTransaction() {
        System.out.println("Enter id - user, id - transaction");
        int idUser = scanner.nextInt();
        String uuid = scanner.next();
        int sum = transactionsService.getUsersArrayList().getUserID(idUser).getList().getTransaction(uuid).getSum();
        User user;
        if (transactionsService.getUsersArrayList().getUserID(idUser).getList().getTransaction(uuid).getRecipient().getId() == idUser){
            user = transactionsService.getUsersArrayList().getUserID(idUser).getList().getTransaction(uuid).getSender();
        }else {
            user = transactionsService.getUsersArrayList().getUserID(idUser).getList().getTransaction(uuid).getRecipient();
        }
        transactionsService.getUsersArrayList().getUserID(idUser).getList().delTransaction(uuid);
        System.out.println("Transaction to " + user.getName() + " (id = " + user.getId() + ") " + sum + " deleted");
    }

    @Override
    public void falseTransaction() {
        User userHasTransaction, userHasNotTransaction;
        System.out.println("Check result:");
        for(int i = 0; i < transactionsService.checkTransaction().length; i++){
            if (transactionsService.checkTransaction()[i].getRecipient().getList().hasTransaction(transactionsService.checkTransaction()[i].getUuid())){
                userHasTransaction = transactionsService.checkTransaction()[i].getRecipient();
                userHasNotTransaction = transactionsService.checkTransaction()[i].getSender();
            } else {
                userHasNotTransaction = transactionsService.checkTransaction()[i].getRecipient();
                userHasTransaction = transactionsService.checkTransaction()[i].getSender();
            }

            System.out.print(userHasTransaction.getName() + "(id = " + userHasTransaction.getId() + ") ");
            System.out.print("has not confirmed transaction " + transactionsService.checkTransaction()[i].getUuid());
            System.out.print("with " + userHasNotTransaction.getName() + "(id = " + userHasNotTransaction.getId() + ") ");
            System.out.println(" at sum " + transactionsService.checkTransaction()[i].getSum());
        }
    }
}
