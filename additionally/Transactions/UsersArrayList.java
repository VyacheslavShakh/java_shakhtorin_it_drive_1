package additionally.paypal;

public class UsersArrayList implements UserList {
    private User[] users = new User[10];
    int count = 0;

    @Override
    public void addUser(User user){
        if (user.getId() >= users.length){
            User[] temp = users;
            users = new User[temp.length+temp.length/2];
            for (int i = 0; i < temp.length; i++){
                users[i] = temp[i];
            }


        }
        users[user.getId()-1] = user;
        count++;
    }

    @Override
    public  User getUserID(int id){
        return users[id-1];
    }

    @Override
    public User getUser(int index) {
        return users[index];
    }

    @Override
    public int getCountUsers() {
        return count;
    }
}
