package additionally.paypal;

public class UserIdsGenerator {
    private int lastID;
    private static UserIdsGenerator instance;

    private UserIdsGenerator() {
    }

    public static UserIdsGenerator getInstance(){
        if (instance == null){
            instance = new UserIdsGenerator();
        }
        return instance;
    }

    public int generateID(){
        lastID++;
        return lastID;
    }


}

