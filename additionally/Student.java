package additionally.five;

public class Student {
    private String name;
    private ClassWork[] classPlane = new ClassWork[40];

    public Student(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public ClassWork getClassPlane(int i) {
        return classPlane[i];
    }

    public void setClassPlane(ClassWork classPlane, int i) {
        this.classPlane[i] = classPlane;
    }


}
