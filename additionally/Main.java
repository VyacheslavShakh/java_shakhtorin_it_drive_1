package additionally.five;



public class Main {
    public static void main(String[] args) {

        Timetable timetable = new Timetable("Math");
        timetable.enterLessons();
        StudyMonth studyMonth = new StudyMonth(1,30);
        Journal journal = new Journal();
        journal.enterListStudents();
        journal.createClassWorks(timetable,studyMonth);
        journal.enterTraffic();
        journal.printJournal();


    }
}
