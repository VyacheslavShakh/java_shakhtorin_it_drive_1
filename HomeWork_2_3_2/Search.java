public class Main {
    public static String search(int arr[], int x) {
        int left = 0;
        int right = arr.length - 1;
        int mid;
        boolean find = false;
        int i = -1;
        while (left <= right) {
            mid = (left + right) / 2;
            if (x > arr[mid]) {
                left = mid + 1;
            } else if (x < arr[mid]) {
                right = mid - 1;
            } else {
                find = true;
                i = mid;
                break;
            }
        }
        if (find == false) {
            i = -1;
        }
        return i;
    }
}