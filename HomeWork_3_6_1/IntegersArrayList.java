package Lists;

public class IntegersArrayList {
    private final int MAX_COUNT = 10;
    private int[] elements;
    private int count = 0;
    private int last = 0;

    public IntegersArrayList() {
        elements = new int[MAX_COUNT];
    }

    public void add(int element) {
        if (elements[count] == 0) {
            if (count < MAX_COUNT) {
                elements[count] = element;
                count++;
                if (count > last) last = count-1;
            } else System.err.println("Out of bound");
        } else {
            count++;
            this.add(element);
        }
    }

    public void add(int element, int index) {
        if (index < MAX_COUNT) {
            elements[index] = element;
            if (index > last) last = index;
        }
    }

    public void remove(int index) {
        if (index < last) {
            for (int i = index; i < last; i++) {
                elements[i] = elements[i + 1];
            }
            elements[last] = 0;
            last--;
            if (index < count) count--;
        }
    }

    public void reverse() {
        for (int i = 0; i < (last+1) / 2; i++) {
            int tepm = elements[i];
            elements[i] = elements[last - i];
            elements[last - i] = tepm;
        }
    }

    public int get (int index) {
        return elements[index];
    }
}
