package Builder;

public class Student {
    private String firstName;
    private String lastName;


    public static Builder builder() {
        Builder builder = new Builder();
        return builder;
    }

    public static class Builder {


        private String fN;
        private String lN;


        public Builder addFirstName(String firstName) {
            this.fN = firstName;
            return this;
        }

        public Builder addLastName(String lastName) {
            this.lN = lastName;
            return this;
        }

        public Student build() {
            Student student = new Student();
            student.firstName = this.fN;
            student.lastName = this.lN;
            return student;
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}

