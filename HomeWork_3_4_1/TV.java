public class TV {
    private RemoteController remoteController;
    private Channel[] channels;
    private final int MAX_CHANAL = 10;
    private int countChennel = 0;
    private char[] tvname;

    TV(char[]tvname){
        this.tvname = tvname;
        channels = new Channel[4];
        countChennel = 4;
        for (int i = 0; i<channels.length; i++){
            channels[i] = new Channel("Channel № ".toCharArray(),i+1);
        }
    }

    void setRemoteController(RemoteController remoteController){
        this.remoteController = remoteController;
        remoteController.setTV(this);
    }

    void addChannel(Channel channel){
        if (countChennel<MAX_CHANAL){
        channels[countChennel] = channel;
        countChennel++;
        }
    }
    void display (int currentChannel){
        if (currentChannel<countChennel) {
            System.out.print("Now TV ");
            for (int i = 0; i < tvname.length; i++){
                System.out.print(tvname[i]);
            }
            System.out.print(" show ");
            channels[currentChannel].showChannel();

        }
    }

}
