public class Program {
    private char[] programName;

    Program(char[] programName, int nom){
        programName[programName.length-1] = (char)(nom+48);
        this.programName = programName;
    }

    public void showProgram (){
        for (int i = 0; i < programName.length; i++){
            System.out.print(programName[i]);
        }
    }
}
