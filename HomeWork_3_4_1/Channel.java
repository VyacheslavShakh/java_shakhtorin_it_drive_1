import java.util.Random;

public class Channel {
    private char[] nameChannel;
    private Program[] listProgram;
    private int countPrograms;
    private int numChannel;
    Random random = new Random();

    Channel(char[] nameChannel, int num){
        nameChannel[nameChannel.length-1] = (char)(num+48);
        numChannel = num;
        this.nameChannel = nameChannel;
        countPrograms = random.nextInt(4)+4;
        listProgram = new Program[countPrograms];
        for (int i = 0; i < listProgram.length; i++){
            listProgram[i] = new Program("Program № ".toCharArray(),i);
        }
    }

    public void showChannel(){
        listProgram[random.nextInt(countPrograms)].showProgram();
        System.out.print(" on channel ");
        for (int i = 0; i < nameChannel.length; i++){
            System.out.print(nameChannel[i]);
        }
    }



    public char[] getNameChannel(){
        return nameChannel;
    }
}
