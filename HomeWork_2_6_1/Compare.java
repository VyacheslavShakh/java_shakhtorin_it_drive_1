import java.util.Scanner;

public class Chars {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your String");
        char str1[] = scanner.nextLine().toCharArray();
        char str2[] = scanner.nextLine().toCharArray();

        System.out.println(stringCompare(str1, str2));


    }

    public static int stringCompare(char[] s1, char[] s2) {
        int lenString;
        int compare = 0;
        if (s1.length <= s2.length) {
            lenString = s1.length;
        } else {
            lenString = s2.length;

        }


        for (int i = 0; i < lenString; i++) {
            if (s1[i] > s2[i]) {
                compare = -1;
                break;
            } else if (s1[i] < s2[i]) {
                compare = 1;
                break;
            } else {
                compare = 0;
            }
            if (compare == 0) {
                if (s1.length < s2.length) {
                    compare = 1;
                } else if (s1.length > s2.length){
                    compare = -1;

                }
            }

        }
        return compare;
    }


}


