import java.util.Scanner;

public class Main {

    public static double f (double x){
        double f=x*x;
        return f;
    }

    public static double integral (double a, double b, int n){
        double h = (b-a)/n;
        double answer=0;
        for (double i = a; i < b; i =i+h){
            double si = h*f(i);
            answer = answer + si;
        }
        return answer;
    }

    public static double simpson (double a, double b, int n){
        double h = (b-a)/n;
        double answer = 0;
        for (double x = a; x < b; x=x+2*h){
            double summ = f(x-h)+4*f(x)+f(x+h);
            answer = answer+summ;
        }
        return h*answer/3;
    }


       public static void main(String[] args) {
           Scanner scanner = new Scanner(System.in);
           System.out.println("Enter the boundary values");
           double a = scanner.nextDouble();
           double b = scanner.nextDouble();
           System.out.println("Iterations  |  Method of rectangles  |  Method Simpsona");

           for (int i =10; i<=1000000; i=i*10){
               System.out.print(i + "                 \t");
               System.out.print(integral(a,b,i)+"       \t");
               System.out.println(simpson(a,b,i));
           }

    }
}

