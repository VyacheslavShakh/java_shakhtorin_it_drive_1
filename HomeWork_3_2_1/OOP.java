import java.util.Random;

public class OOP {
    public static void main(String[] args) {
        Human[] humans = new Human[100];
        Random random = new Random();
        for (int i = 0; i < humans.length; i++) {
            humans[i] = new Human();
            humans[i].setAge(random.nextInt(90));
            humans[i].randomName();
        }
        System.out.println("most people age is - " + moreOftenAge(humans));


        for (int i = 0; i < humans.length; i++){
            System.out.print("Human № " + i + " name is ");
            humans[i].printName();
            System.out.println(" age is " + humans[i].age);
        }

    }
    public static int moreOftenAge (Human[] inHumans){
        int[] ages = new int[90];
        for (int i = 0; i < inHumans.length; i++){
            ages[inHumans[i].age]++;
        }
        int ageCase = 0;
        int oftenAge = 0;
        for (int i = 0; i<ages.length; i++){
            if (ages[i]>ageCase){
                ageCase = ages[i];
                oftenAge = i;
            }
        }
        return oftenAge;
    }
}