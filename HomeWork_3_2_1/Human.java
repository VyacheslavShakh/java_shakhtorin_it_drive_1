import java.util.Random;

public class Human {
    char[] name;
    int age;

    public void setName(char[] newName) {
        name = newName;
    }

    public void setAge(int newAge) {
        age = newAge;
    }

    public void printName (){
        for (int i = 0; i < name.length; i++){
            System.out.print(name[i]);
        }
    }

    public void randomName() {
        Random random = new Random();
        int lengthName = random.nextInt(6) + 3;
        char[] name = new char[lengthName];
        name[0] = (char)(random.nextInt(27)+65);
        for (int i = 1; i < name.length; i++) {
            name[i] = (char)(random.nextInt(27)+97);
            this.name = name;
        }
    }
}
