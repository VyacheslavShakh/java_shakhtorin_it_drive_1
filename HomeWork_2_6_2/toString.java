import java.util.Scanner;

public class Chars {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your String");
        char str1[] = scanner.nextLine().toCharArray();


        System.out.println(toInt(str1));


    }


    public static int toInt(char[] arr) {
        int value = 0;

        for (int i = 0; i < arr.length; i++) {
            value = (value + arr[i] - 48) * 10;
        }

        return value / 10;
    }


}


