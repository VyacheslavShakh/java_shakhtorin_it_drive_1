package Lists;

public class IntegersLinkedList {
    private Node first;
    private int count;

    class Node {
        private int value;
        private Node next;

        private Node(int value) {
            this.value = value;
        }

        int getValue() {
            return value;
        }

        private void setValue(int value) {
            this.value = value;
        }

        Node getNext() {
            return next;
        }

        private void setNext(Node next) {
            this.next = next;
        }
    }

    class LinkedListIterator {
        Node carrentNode;

        public Node getCarrentNode() {
            return carrentNode;
        }

        LinkedListIterator() {
            this.carrentNode = first;
        }

        Node next() {
            Node temp = carrentNode;
            carrentNode = carrentNode.getNext();
            return temp;
        }

        boolean hasNext() {
            return carrentNode != null;
        }

    }

    IntegersLinkedList() {
        count = 0;
    }

    public void add(int value) {
        Node newNode = new Node(value);
        if (first == null) {
            this.first = newNode;
        } else {
            Node carrent = first;
            while (carrent.getNext() != null) {
                carrent = carrent.getNext();
            }
            carrent.setNext(newNode);
        }
        count++;
    }

    public void add(int value, int index) {
        Node newNode = new Node(value);
        if (index <= count) {
            if (index == 0) {
                this.first = newNode;
            } else {
                Node carrent = first;
                for (int i = 0; i < index; i++) {
                    carrent = carrent.getNext();
                }
                carrent.setValue(value);
            }
        }
    }

    public int get(int index) {
        Node searchingNode = first;
        if (index <= count) {
            for (int i = 0; i < index; i++) {
                searchingNode = searchingNode.getNext();
            }
        }
        return searchingNode.getValue();
    }

    public void remove(int index) {
        Node carrent = first;
        for (int i = 0; i < index - 1; i++) {
            carrent = carrent.getNext();
        }
        carrent.setNext(carrent.getNext().getNext());
        count--;
    }

    public void reverse() {
        Node previous = first;
        Node carrent = previous.getNext();
        Node next = carrent.getNext();

        do {
            if (previous == first) previous.setNext(null);
            carrent.setNext(previous);

            previous = carrent;
            carrent = next;
            if (next != null) {
                next = next.getNext();
            }
        } while (carrent != null);
        this.first = previous;
    }


}
