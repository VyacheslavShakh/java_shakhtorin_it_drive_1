package Lists;

public class Main {
    public static void main(String[] args) {

        IntegersLinkedList linkList = new IntegersLinkedList();

        linkList.add(1);
        linkList.add(2);
        linkList.add(3);
        linkList.add(4);
        linkList.add(5, 1);
        linkList.remove(2);
        linkList.add(6);
        linkList.add(7);
        linkList.add(8);
        linkList.add(9);

        IntegersLinkedList.LinkedListIterator iterator = linkList.new LinkedListIterator();

        do {
            System.out.println(iterator.next().getValue());
        } while (iterator.hasNext());

        linkList.reverse();

        System.out.println();

        while (iterator.hasNext()) {
            System.out.println(iterator.next().getValue());
        }


    }
}
