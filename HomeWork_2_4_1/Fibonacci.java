public class Fibonatchi {


    public static int fib(int n, int x1, int x2) {

        int result;

        if ((n == 1) | (n == 2)) {
            return 1;
        }

        int x3 = x2 + x1;

        x1 = x2;

        x2 = x3;

        result = fib(n - 1, x1, x2);

        if (x2 > result) {
            result = x2;
        }

        return result;
    }

    public static void main(String[] args) {
        System.out.println(fib(6, 1, 1));

    }
}
